# 实验4：PL/SQL语言打印杨辉三角

- 学号：202010414419   姓名：王鑫涛   班级：软件工程4班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```

以上代码是一个PL/SQL程序，用于输出杨辉三角。程序中使用了一个 VARRAY 类型的数组 `t_number`，用于存储每一行的数字。

程序中通过循环来打印每一行的数字，每行数字的数量从第一行开始逐行递增。具体来说，程序初始化了数组的前两个元素为1，然后从第3行开始循环，对于每一行，先设置该行第一个元素为1，然后通过循环计算出剩余元素的值，最后将该行的所有元素打印输出。

![](./PNG1.png)

## 创建杨辉三角存储过程

```sql
CREATE OR REPLACE PROCEDURE YHTriangle(N IN INTEGER) AS
    type t_number is varray (100) of integer not null; --数组
    i integer;
    j integer;
    spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
    rowArray t_number := t_number();
BEGIN
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1, 9, ' '));--先打印第2行
    dbms_output.put(rpad(1, 9, ' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j), 9, ' '));--打印数字并在后面补空格
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```

这段代码定义了一个名为 `YHTriangle`的存储过程，它将打印一个 N 行的杨辉三角形。

在存储过程的开头，定义了一个类型为 varray 的数组 rowArray，用于存储杨辉三角形中每一行的数字。数组大小被设置为 100，因为它最大可能有 N 个元素。在存储过程中，使用了一个循环，将数组的大小初始化为 N。

在循环开始之前，存储过程会先打印杨辉三角形的第一行，接着打印第二行的第一个数字 1 和第二个数字 1，并在最后加上一个换行符。接下来，存储过程开始循环从第三行到第 N 行，每次循环都计算并打印一行数字。在循环的开头，将数组 "rowArray" 的第 i 个元素设置为 1。然后，通过一个 while 循环来计算每行中间的数字。循环中的变量 j 从 i-1 开始递减，直到 j=2 为止。在循环内部，存储过程将 rowArray(j) 的值设置为它自身加上 rowArray(j-1) 的值，以计算当前行的数字。最后，在循环的末尾，通过一个 for 循环打印当前行的数字。每次循环结束后，存储过程会打印一个换行符来开始新的一行。当存储过程执行完成后，将会生成一个 N 行的杨辉三角形，输出到控制台上。

请注意，此存储过程使用了PL/SQL中的`dbms_output`包来在控制台上打印输出结果。如果想要在其他地方使用这些数据，需要对此存储过程进行修改，将输出结果传递给其他程序或存储到数据库表中。

![](./PNG2.png)

## 打印杨辉三角

```sql
SET SERVEROUTPUT ON;
BEGIN
YHTriangle(20);
END;
```

这段代码调用了一个名为`YHTriangle`的存储过程，并传入参数`N=20`，该过程的作用是打印出一个杨辉三角（YHTriangle）的前20行。![](./PNG3.png)

## 实验总结

本次实验的目标是将杨辉三角的生成代码转化为一个名为 `YHTriangle` 的存储过程，并在 HR 用户下创建这个存储过程。该存储过程接受一个整数参数 N，并打印出 N 行杨辉三角。

首先，我们对杨辉三角的生成代码进行了修改，以适应存储过程的需求。在存储过程中，我们使用了一个名为 t_number 的 Varray 类型来存储每一行的数字，使用 `dbms_output.put_line` 和 `dbms_output.put` 方法将数字打印到控制台上。在初始化数组和生成每一行数字时，我们使用了循环结构和条件语句等基本的编程概念。

接下来，我们使用了 SQL 语句创建了名为 `YHTriangle` 的存储过程。我们使用 `create or replace procedure` 语句创建一个存储过程，指定输入参数和过程体，并在过程体内实现杨辉三角的生成和打印。

最后，我们在 SQLPlus 中测试了 `YHTriangle` 存储过程的功能。我们输入了一个整数参数，调用存储过程并打印出杨辉三角的前 N 行。在测试中，我们发现存储过程能够正确地打印出杨辉三角，并且能够处理输入参数为较大数值的情况。

总的来说，本次实验让我们进一步熟悉了 PL/SQL 的编写和存储过程的创建方法，同时也加深了我们对基本编程概念的理解。通过实践，我们深刻体会到了编程的乐趣和挑战，并认识到了不断学习和实践的重要性。
