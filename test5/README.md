# 实验5：包，过程，函数的用法

- 学号：202010414419   姓名：王鑫涛   班级：软件工程4班

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

![](./PNG1.png)

## 脚本代码

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![](./PNG2.png)

![](./PNG3.png)

## 测试

函数Get_SalaryAmount()测试方法：

```sql

select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

![](./PNG4.png)

过程Get_Employees()测试代码：

```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```

![](./PNG5.png)

## 实验总结

本次实验涉及到PL/SQL语言结构、变量和常量的声明和使用方法，以及包、过程、函数的用法。

在PL/SQL中，变量和常量可以通过DECLARE语句进行声明，并且可以使用不同的数据类型。在使用变量和常量时，可以对其进行赋值、计算等操作。

包、过程、函数则是PL/SQL的重要组成部分，它们都可以通过CREATE语句进行创建，并且可以包含变量、常量、游标、异常等内容。包是一个命名空间，可以包含一个或多个过程或函数，可以使用PACKAGE、PACKAGE BODY语句进行创建和实现。过程是一个可重用的代码块，用于执行一些特定的任务或操作，可以使用PROCEDURE语句进行创建。函数也是一个可重用的代码块，用于执行一些特定的任务或操作，并且可以返回一个值，可以使用FUNCTION语句进行创建。

在使用包、过程、函数时，可以通过CALL语句进行调用，并且可以传递参数。其中，包和函数可以使用RETURN语句返回结果，过程则是通过OUT参数返回结果。

本次实验通过实践操作，深入学习了PL/SQL语言结构、变量和常量的声明和使用方法，以及包、过程、函数的用法。这些内容在实际的数据库编程中都有广泛的应用，对于提高编程效率和代码质量都具有重要作用。
