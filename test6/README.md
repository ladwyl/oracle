﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](../test6)

#### 姓名：王鑫涛
#### 学号：202010414419
#### 班级：20级软件工程4班

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|


## 总体设计方案
### 数据库表设计
表空间设计方案：

1. SYSTEM 表空间：用于存储系统级对象，如数据字典、系统表等。
2. USERS 表空间：用于存储用户创建的表和索引。

表设计方案：

1. 商品表（Products）：
   - 列：商品ID（ProductID，主键）、商品名称（ProductName）、价格（Price）、库存数量（Quantity）。
2. 客户表（Customers）：
   - 列：客户ID（CustomerID，主键）、客户姓名（CustomerName）、联系方式（Contact）、地址（Address）。
3. 订单表（Orders）：
   - 列：订单ID（OrderID，主键）、客户ID（CustomerID，外键参考Customers表）、订单日期（OrderDate）、订单总金额（TotalAmount）。
4. 订单详情表（OrderDetails）：
   - 列：订单详情ID（DetailID，主键）、订单ID（OrderID，外键参考Orders表）、商品ID（ProductID，外键参考Products表）、商品数量（Quantity）、商品单价（UnitPrice）、总金额（TotalAmount）。

### 权限及用户分配设计
创建了两个用户：销售用户（sales_user）和管理员用户（admin_user）。销售用户被分配了销售角色，而管理员用户被分配了管理员角色。
创建了两个角色：销售角色（sales_role）和管理员角色（admin_role）。销售角色具有对商品表、客户表和订单表的查询、插入和更新权限，而管理员角色具有对所有表的查询、插入、更新和删除权限。

### 存储过程设计
1. `calculate_order_total`：计算指定订单的总金额。
2. `update_product_quantity`：更新指定商品的库存数量。
3. `get_customer_total_spent`：获取指定客户的累计消费金额。
4. `check_product_availability`：检查指定商品的库存是否充足。

### 备份方案设计
使用 RMAN (Recovery Manager) 工具进行 Oracle 数据库备份和管理。


## 详细过程
### 1.创建用户,授予用户权限
```sql
--创建一个名为wxt的用户，密码为123
GRANT USER wxt IDENTIFIED BY 123;  
--授予wxt权限
GRANT CONNECT,RESOURCE,CREATE SESSION TO wxt;
```
创建名为"wxt"的用户授予连接数据库的权限，并将其密码设置为"123"。并为wxt用户授予了额外的权限，包括CONNECT、RESOURCE和CREATE SESSION  
![](./step1.png)

### 2.创建表空间
```sql
-- 创建系统表空间
CREATE TABLESPACE sale_sys
  DATAFILE 'sale_sys.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED
  LOGGING
  EXTENT MANAGEMENT LOCAL AUTOALLOCATE
  SEGMENT SPACE MANAGEMENT AUTO;
```
创建了一个名为"sale_sys"的系统表空间。它指定了一个名为"sale_sys.dbf"的数据文件，初始大小为100MB，并且允许自动扩展，每次扩展100MB，最大大小为无限。此外，它启用了日志记录、本地自动分配的区管理以及自动段空间管理。

```sql
-- 创建数据表空间
CREATE TABLESPACE sale_data
  DATAFILE 'sale_data.dbf'
  SIZE 500M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED
  LOGGING
  EXTENT MANAGEMENT LOCAL AUTOALLOCATE
  SEGMENT SPACE MANAGEMENT AUTO;
```
创建了一个名为"sale_data"的数据表空间。它指定了一个名为"sale_data.dbf"的数据文件，初始大小为500MB，并且允许自动扩展，每次扩展100MB，最大大小为无限。与系统表空间相似，它也启用了日志记录、本地自动分配的区管理以及自动段空间管理。  
![](./step2.png)

### 3.创建数据库

![](./step3.png) 

### 4.创建表

```sql
-- 创建商品表
CREATE TABLE products (
  product_id   NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  price        NUMBER(10, 2),
  quantity     NUMBER
)
TABLESPACE sale_data;
```
创建了一个名为"products"的表。它包含了一些列，包括商品ID（product_id）、商品名称（product_name）、价格（price）和库存数量（quantity）。商品ID被定义为主键。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建客户表
CREATE TABLE customers (
  customer_id   NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100),
  contact       VARCHAR2(100),
  address       VARCHAR2(200)
)
TABLESPACE sale_data;
```
创建了一个名为"customers"的表。它包含了一些列，包括客户ID（customer_id）、客户名称（customer_name）、联系方式（contact）和地址（address）。客户ID被定义为主键。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建订单表
CREATE TABLE orders (
  order_id      NUMBER PRIMARY KEY,
  customer_id   NUMBER,
  order_date    DATE,
  total_amount  NUMBER(10, 2),
  CONSTRAINT fk_orders_customers
    FOREIGN KEY (customer_id)
    REFERENCES customers (customer_id)
)
TABLESPACE sale_data;
```
创建了一个名为"orders"的表。它包含了一些列，包括订单ID（order_id）、客户ID（customer_id）、订单日期（order_date）和总金额（total_amount）。订单ID被定义为主键，并且还创建了一个外键约束，将订单表中的customer_id列与customers表中的customer_id列关联起来。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建订单详情表
CREATE TABLE order_details (
  detail_id     NUMBER PRIMARY KEY,
  order_id      NUMBER,
  product_id    NUMBER,
  quantity      NUMBER,
  unit_price    NUMBER(10, 2),
  total_amount  NUMBER(10, 2),
  CONSTRAINT fk_order_details_orders
    FOREIGN KEY (order_id)
    REFERENCES orders (order_id),
  CONSTRAINT fk_order_details_products
    FOREIGN KEY (product_id)
    REFERENCES products (product_id)
)
TABLESPACE sale_data;
```
创建了一个名为"order_details"的表。它包含了一些列，包括订单详情ID（detail_id）、订单ID（order_id）、商品ID（product_id）、商品数量（quantity）、商品单价（unit_price）和总金额（total_amount）。订单详情ID被定义为主键，并且还创建了两个外键约束，将订单详情表中的order_id列与orders表中的order_id列关联起来，将order_details表中的product_id列与products表中的product_id列关联起来。该表将存储在名为"sale_data"的表空间中。
![](./step4.png)

### 5.在表中插入100000条数据
```sql
-- 插入商品数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO products (product_id, product_name, price, quantity)
VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
END LOOP;
COMMIT;
END;
/

-- 插入客户数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO customers (customer_id, customer_name, contact, address)
VALUES (i, 'Customer ' || i, 'Contact ' || i, 'Address ' || i);
END LOOP;
COMMIT;
END;
/

-- 插入订单数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO orders (order_id, customer_id, order_date, total_amount)
VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), SYSDATE - i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
END LOOP;
COMMIT;
END;
/

-- 插入订单详情数据
BEGIN
FOR i IN 1..100000 LOOP
DECLARE
product_id NUMBER;
BEGIN
SELECT product_id
INTO product_id
FROM products
WHERE product_id = ROUND(DBMS_RANDOM.VALUE(1, 100000), 0);
  INSERT INTO order_details (detail_id, order_id, product_id, quantity, unit_price, total_amount)
  VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), product_id, ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    CONTINUE;
END;
END LOOP;
COMMIT;
END;
/
```
通过 FOR 循环从 1 到 100,000 遍历，逐个插入数据；使用 `DBMS_RANDOM.VALUE` 生成随机的价格数量值；每次循环插入一条记录到每个表中；在订单详情表插入数据时，在每次循环内部，通过查询 `products` 表来获取一个随机的商品ID，使用 `DBMS_RANDOM.VALUE` 生成随机的订单详情相关数据，尝试插入一条记录到 `order_details` 表中，如果找不到匹配的商品ID，则通过异常处理跳过该次循环。
![](./step5.png)

### 6.创建角色和权限分配
```sql
-- 创建管理员角色
CREATE ROLE admin_role;

-- 创建销售角色
CREATE ROLE sales_role;
```
创建两个角色一个管理员角色（admin_role），一个销售员角色（sales_role）
![](./step6.png)

```sql
-- 授予管理员角色权限
GRANT CREATE SESSION TO admin_role;
GRANT CREATE TABLE TO admin_role;
GRANT CREATE SEQUENCE TO admin_role;
GRANT CREATE VIEW TO admin_role;
GRANT ALL PRIVILEGES ON products TO admin_role;
GRANT ALL PRIVILEGES ON customers TO admin_role;
GRANT ALL PRIVILEGES ON orders TO admin_role;
GRANT ALL PRIVILEGES ON order_details TO admin_role;
```
管理员角色（admin_role）被授予创建会话、创建表、创建序列和创建视图的权限。  
![](./step7.png)

```sql
-- 授予销售员角色权限
GRANT CREATE SESSION TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sales_role;
```

销售员角色（sales_role）被授予对商品、客户、订单和订单详情表的选择、插入、更新和删除权限。

![](./step8.png)

### 7.创建用户

```sql
-- 创建销售用户
CREATE USER sales_user IDENTIFIED BY password123;
-- 分配销售角色给销售用户
GRANT sales_role TO sales_user;

-- 创建管理员用户
CREATE USER admin_user IDENTIFIED BY password456;
-- 分配管理员角色给管理员用户
GRANT admin_role TO admin_user;
```

首先创建了两个角色：销售角色（sales_role）和管理员角色（admin_role）。销售角色具有对商品表、客户表和订单表的查询、插入和更新权限，而管理员角色具有对所有表的查询、插入、更新和删除权限。

然后又创建了两个用户：销售用户（sales_user）和管理员用户（admin_user）。销售用户被分配了销售角色，而管理员用户被分配了管理员角色。

![](./step9.png)

### 8.创建程序包
创建了一个名为`sales_package`的程序包。程序包中包含了四个子程序：两个存储过程和两个函数。
1. 存储过程`calculate_order_total`（计算订单总金额）：
   - 输入参数：`p_order_id`（订单ID）
   - 输出参数：`p_total_amount`（订单总金额）
   - 功能：根据给定的订单ID，查询订单详情表（可能是`order_details`表）中相应订单的商品数量和单价，并计算出订单的总金额。将计算结果存储在输出参数`p_total_amount`中。
2. 存储过程`update_product_quantity`（更新商品库存数量）：
   - 输入参数：`p_product_id`（商品ID）、`p_quantity_delta`（库存数量变化量）
   - 功能：根据给定的商品ID，将商品表（可能是`products`表）中对应商品的库存数量增加或减少指定的库存数量变化量`p_quantity_delta`。
3. 函数`get_customer_total_spent`（获取客户的累计消费金额）：
   - 输入参数：`p_customer_id`（客户ID）
   - 返回值：客户的累计消费金额
   - 功能：根据给定的客户ID，查询订单表（可能是`orders`表）和订单详情表（可能是`order_details`表），计算指定客户的累计消费金额。将计算结果作为函数的返回值。
4. 函数`check_product_availability`（检查商品库存是否充足）：
   - 输入参数：`p_product_id`（商品ID）、`p_quantity_needed`（所需商品数量）
   - 返回值：布尔值，表示商品库存是否充足（TRUE表示充足，FALSE表示不充足）
   - 功能：根据给定的商品ID，查询商品表（可能是`products`表）中对应商品的库存数量，并与所需商品数量`p_quantity_needed`进行比较，判断商品库存是否充足。如果库存充足，返回TRUE；否则，返回FALSE。

```sql
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_package AS
  -- 存储过程：计算订单总金额
  PROCEDURE calculate_order_total(
    p_order_id IN NUMBER,
    p_total_amount OUT NUMBER
  );
  
  -- 存储过程：更新商品库存数量
  PROCEDURE update_product_quantity(
    p_product_id IN NUMBER,
    p_quantity_delta IN NUMBER
  );
  
  -- 函数：获取客户的累计消费金额
  FUNCTION get_customer_total_spent(
    p_customer_id IN NUMBER
  ) RETURN NUMBER;
  
  -- 函数：检查商品库存是否充足
  FUNCTION check_product_availability(
    p_product_id IN NUMBER,
    p_quantity_needed IN NUMBER
  ) RETURN BOOLEAN;
END sales_package;
/

-- 创建包体
CREATE OR REPLACE PACKAGE BODY sales_package AS
  -- 存储过程：计算订单总金额
  PROCEDURE calculate_order_total(
    p_order_id IN NUMBER,
    p_total_amount OUT NUMBER
  ) IS
  BEGIN
    SELECT SUM(od.quantity * od.unit_price)
    INTO p_total_amount
    FROM order_details od
    WHERE od.order_id = p_order_id;
  END calculate_order_total;
  
  -- 存储过程：更新商品库存数量
  PROCEDURE update_product_quantity(
    p_product_id IN NUMBER,
    p_quantity_delta IN NUMBER
  ) IS
  BEGIN
    UPDATE products
    SET quantity = quantity + p_quantity_delta
    WHERE product_id = p_product_id;
  END update_product_quantity;
  
  -- 函数：获取客户的累计消费金额
  FUNCTION get_customer_total_spent(
    p_customer_id IN NUMBER
  ) RETURN NUMBER IS
    l_total_spent NUMBER;
  BEGIN
    SELECT SUM(od.total_amount)
    INTO l_total_spent
    FROM orders o
    JOIN order_details od ON o.order_id = od.order_id
    WHERE o.customer_id = p_customer_id;
    
    RETURN l_total_spent;
  END get_customer_total_spent;
  
  -- 函数：检查商品库存是否充足
  FUNCTION check_product_availability(
    p_product_id IN NUMBER,
    p_quantity_needed IN NUMBER
  ) RETURN BOOLEAN IS
    l_available_quantity NUMBER;
  BEGIN
    SELECT quantity
    INTO l_available_quantity
    FROM products
    WHERE product_id = p_product_id;
    
    IF l_available_quantity >= p_quantity_needed THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END check_product_availability;
END sales_package;
/
```
![](./step10.png)
### 9.调用存储过程
```sql
SET SERVEROUTPUT ON;
-- 调用存储过程：计算订单总金额
DECLARE
  l_total_amount NUMBER;
BEGIN
  sales_package.calculate_order_total(1, l_total_amount);
  DBMS_OUTPUT.PUT_LINE('Order Total Amount: ' || l_total_amount);
END;
/

-- 调用存储过程：更新商品库存数量
BEGIN
  sales_package.update_product_quantity(1, -5); -- 减少5个商品的库存数量
  DBMS_OUTPUT.PUT_LINE('Product quantity updated.');
END;
/

-- 调用函数：获取客户的累计消费金额
DECLARE
  l_total_spent NUMBER;
BEGIN
  l_total_spent := sales_package.get_customer_total_spent(1);
  DBMS_OUTPUT.PUT_LINE('Customer Total Spent: ' || l_total_spent);
END;
/

-- 调用函数：检查商品库存是否充足
DECLARE
  l_product_available BOOLEAN;
BEGIN
  l_product_available := sales_package.check_product_availability(1, 10);
  IF l_product_available THEN
    DBMS_OUTPUT.PUT_LINE('Product is available.');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Product is not available.');
  END IF;
END;
/
```
![](./step11.png)
### 10.备份方案
```sql
-- 登录到数据库
sqlplus / as sysdba

-- 关闭数据库
SHUTDOWN IMMEDIATE

-- 启动数据库实例并挂载数据库
STARTUP MOUNT

-- 将数据库切换到归档日志模式
ALTER DATABASE ARCHIVELOG;

-- 打开数据库
ALTER DATABASE OPEN;
```
![](./step12.png)
```sql
-- 使用 RMAN 工具进行备份
rman target /

-- 显示 RMAN 的当前配置和参数设置
SHOW ALL;

-- 执行完整数据库备份操作
BACKUP DATABASE;
```
![](./step13.png)

![](./step14.png)

```sql
-- 列出已备份的备份集信息
LIST BACKUP;
```

![](./step15.png)

## 实验总结
在本次实验中，我们设计了一个基于Oracle数据库的商品销售系统，并完成了数据库的设计、表空间的创建、数据插入、存储过程和函数的编写以及备份方案的设计。

- 在数据库设计方面，我创建了四张表：products（商品表）、customers（客户表）、orders（订单表）和order_details（订单详情表），并使用两个表空间进行存储，提高了数据库的性能和可维护性。通过合理的表结构设计和数据类型选择，确保了数据的完整性和准确性。

- 数据插入和模拟数据量，我成功插入了十万条虚拟数据，模拟了真实的业务场景，验证了数据库设计的合理性和性能。插入大量数据的过程中，我使用了批量插入的方式，提高了插入的效率，并确保数据的完整性。

- 存储过程和函数的设计方面吗，根据业务需求，我编写了存储过程和函数，实现了复杂的业务逻辑。这些存储过程和函数提供了高效、可复用的代码块，简化了业务逻辑的处理过程，并提高了系统的性能和可维护性。在编写存储过程和函数时，我注重代码的可读性和可维护性，采用了合理的命名规范和注释，方便他人理解和维护代码。

- 在备份方案设计方面，我设计了一个完全备份的数据库备份方案，通过`RMAN`工具实现了定期的全量备份，并将备份文件保存到可靠的存储介质中。通过配置数据库日志的自动归档，我实现了连续保护，能够快速恢复数据并减少数据丢失的风险。

本次实验涵盖了数据库设计、权限和用户管理、程序包设计以及备份方案设计等多个方面，旨在构建一个完整的商品销售系统。通过实验，我深入了解了Oracle数据库的各项功能和特性，并学会了如何设计和实现一个复杂的数据库系统。我了解了表空间的概念和使用方法，掌握了创建表和设置权限的技巧。通过编写程序包，我学会了使用`PL/SQL`语言实现业务逻辑和数据处理操作。

我遇到了一些挑战，如数据量的管理、存储过程和函数的逻辑编写等。通过解决这些挑战，我不仅加深了对Oracle数据库的理解，还提升了数据库设计和管理的技能。我深入了解了基于Oracle数据库的商品销售系统的设计和实现过程。我学会了如何合理地设计表结构、编写存储过程和函数，以及设计备份方案来保护数据的安全。这些知识和技能对于我今后在数据库领域的工作和学习将会非常有帮助。我也意识到数据库设计和管理的重要性，合理的设计和管理能够提高系统的性能、可维护性和可靠性。

我还加深了对Oracle数据库的备份和恢复策略的理解。备份是确保数据安全和可靠性的重要手段之一，而`RMAN`工具提供了便捷的备份和恢复功能。我学会了使用`RMAN`工具进行数据库的全量备份，并设置了定期备份策略，以保证数据的可恢复性。同时，我还了解了备份文件的存储和管理，包括备份文件的位置选择、命名规范以及备份集的管理。

在实验过程中，我遇到了一些挑战和问题。例如，数据库设计过程中需要仔细考虑表之间的关系、主键和外键的设置，以及数据类型的选择等。此外，在编写存储过程和函数时，需要对业务逻辑进行深入的分析和设计，确保逻辑的准确性和高效性。同时，备份方案的设计也需要考虑数据的连续保护和灾难恢复能力，以应对意外情况。

通过本次实验，我不仅掌握了数据库的设计原则和技巧，还学会了使用Oracle数据库的各项功能和工具。我对数据库的整体架构和管理有了更深入的理解，同时也提升了自己的问题解决能力和实践经验。

总的来说，本次实验对我来说是一次有价值的学习和实践机会。我通过设计一个基于Oracle数据库的商品销售系统，从数据库设计到权限管理、存储过程和函数编写，再到备份方案的设计，全面了解了数据库的各个方面。这将对我未来的数据库开发和管理工作起到积极的指导和支持作用。