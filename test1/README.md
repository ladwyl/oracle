# 实验1  SQL语句的执行计划分析与优化指导

姓名： 王鑫涛		学号：202010414419



## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

```
（1）对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
（2）设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的
返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句
是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，
看看该工具有没有给出优化建议。

用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

v_$sesstat, v_$statname 和 v_$session

权限分配过程如下
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

## 查询：	查询出所有在美国的部门中工作的员工的姓名和职位名称。

### 查询1：
```sql
SELECT e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE
FROM EMPLOYEES e
JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID
JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID
JOIN COUNTRIES c ON l.COUNTRY_ID = c.COUNTRY_ID
JOIN JOBS j ON e.JOB_ID = j.JOB_ID
WHERE c.COUNTRY_NAME = 'United States of America';
```

#### 分析：

这个语句使用了多个技巧，从EMPLOYEES表、DEPARTMENTS表、LOCATIONS表、COUNTRIES表和JOBS表中检索员工的名字、姓氏和工作职称，使用JOIN语句将这些表连接起来，以便可以按照DEPARTMENTS表、LOCATIONS表、COUNTRIES表和JOBS表中的相应字段对它们进行关联，使用WHERE子句将结果集限制为仅包括属于"United States of America"国家的员工，最后，查询语句返回所有满足条件的员工的名字、姓氏和工作职称。

#### 输出结果：
```sql
FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Steven		     King
President

Neena		     Kochhar
Administration Vice President

Lex		     De Haan
Administration Vice President


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Jennifer	     Whalen
Administration Assistant

Nancy		     Greenberg
Finance Manager

Daniel		     Faviet
Accountant


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
John		     Chen
Accountant

Ismael		     Sciarra
Accountant

Jose Manuel	     Urman
Accountant


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Luis		     Popp
Accountant

Shelley 	     Higgins
Accounting Manager

William 	     Gietz
Public Accountant


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Den		     Raphaely
Purchasing Manager

Alexander	     Khoo
Purchasing Clerk

Shelli		     Baida
Purchasing Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Sigal		     Tobias
Purchasing Clerk

Guy		     Himuro
Purchasing Clerk

Karen		     Colmenares
Purchasing Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Matthew 	     Weiss
Stock Manager

Adam		     Fripp
Stock Manager

Payam		     Kaufling
Stock Manager


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Shanta		     Vollman
Stock Manager

Kevin		     Mourgos
Stock Manager

Julia		     Nayer
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Irene		     Mikkilineni
Stock Clerk

James		     Landry
Stock Clerk

Steven		     Markle
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Laura		     Bissot
Stock Clerk

Mozhe		     Atkinson
Stock Clerk

James		     Marlow
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
TJ		     Olson
Stock Clerk

Jason		     Mallin
Stock Clerk

Michael 	     Rogers
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Ki		     Gee
Stock Clerk

Hazel		     Philtanker
Stock Clerk

Renske		     Ladwig
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Stephen 	     Stiles
Stock Clerk

John		     Seo
Stock Clerk

Joshua		     Patel
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Trenna		     Rajs
Stock Clerk

Curtis		     Davies
Stock Clerk

Randall 	     Matos
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Peter		     Vargas
Stock Clerk

Winston 	     Taylor
Shipping Clerk

Jean		     Fleaur
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Martha		     Sullivan
Shipping Clerk

Girard		     Geoni
Shipping Clerk

Nandita 	     Sarchand
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Alexis		     Bull
Shipping Clerk

Julia		     Dellinger
Shipping Clerk

Anthony 	     Cabrio
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Kelly		     Chung
Shipping Clerk

Jennifer	     Dilly
Shipping Clerk

Timothy 	     Gates
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Randall 	     Perkins
Shipping Clerk

Sarah		     Bell
Shipping Clerk

Britney 	     Everett
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Samuel		     McCain
Shipping Clerk

Vance		     Jones
Shipping Clerk

Alana		     Walsh
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Kevin		     Feeney
Shipping Clerk

Donald		     OConnell
Shipping Clerk

Douglas 	     Grant
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Alexander	     Hunold
Programmer

Bruce		     Ernst
Programmer

David		     Austin
Programmer


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Valli		     Pataballa
Programmer

Diana		     Lorentz
Programmer


已选择 68 行。


执行计划
----------------------------------------------------------
Plan hash value: 1271915316

--------------------------------------------------------------------------------
------------------------------

| Id  | Operation				 | Name 	     | Rows  | B
ytes | Cost (%CPU)| Time     |

--------------------------------------------------------------------------------
------------------------------

|   0 | SELECT STATEMENT			 |		     |	   8 |
 632 |	   7   (0)| 00:00:01 |

|*  1 |  HASH JOIN				 |		     |	   8 |
 632 |	   7   (0)| 00:00:01 |

|   2 |   NESTED LOOPS				 |		     |	   8 |
 416 |	   4   (0)| 00:00:01 |

|   3 |    NESTED LOOPS 			 |		     |	  20 |
 416 |	   4   (0)| 00:00:01 |

|   4 |     NESTED LOOPS			 |		     |	   2 |
  50 |	   3   (0)| 00:00:01 |

|   5 |      NESTED LOOPS			 |		     |	   2 |
  36 |	   2   (0)| 00:00:01 |

|*  6 |       INDEX FULL SCAN			 | COUNTRY_C_ID_PK   |	   1 |
  12 |	   1   (0)| 00:00:01 |

|   7 |       TABLE ACCESS BY INDEX ROWID BATCHED| LOCATIONS	     |	   2 |
  12 |	   1   (0)| 00:00:01 |

|*  8 |        INDEX RANGE SCAN 		 | LOC_COUNTRY_IX    |	   2 |
     |	   0   (0)| 00:00:01 |

|   9 |      TABLE ACCESS BY INDEX ROWID BATCHED | DEPARTMENTS	     |	   1 |
   7 |	   1   (0)| 00:00:01 |

|* 10 |       INDEX RANGE SCAN			 | DEPT_LOCATION_IX  |	   4 |
     |	   0   (0)| 00:00:01 |

|* 11 |     INDEX RANGE SCAN			 | EMP_DEPARTMENT_IX |	  10 |
     |	   0   (0)| 00:00:01 |

|  12 |    TABLE ACCESS BY INDEX ROWID		 | EMPLOYEES	     |	   4 |
 108 |	   1   (0)| 00:00:01 |

|  13 |   TABLE ACCESS FULL			 | JOBS 	     |	  19 |
 513 |	   3   (0)| 00:00:01 |

--------------------------------------------------------------------------------
------------------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   1 - access("E"."JOB_ID"="J"."JOB_ID")
   6 - filter("C"."COUNTRY_NAME"='United States of America')
   8 - access("L"."COUNTRY_ID"="C"."COUNTRY_ID")
  10 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
  11 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")

Note
-----
   - this is an adaptive plan


统计信息
----------------------------------------------------------
	329  recursive calls
	  0  db block gets
	761  consistent gets
	  7  physical reads
	  0  redo size
       2987  bytes sent via SQL*Net to client
	652  bytes received via SQL*Net from client
	  6  SQL*Net roundtrips to/from client
	 38  sorts (memory)
	  0  sorts (disk)
	 68  rows processed
```

### 查询2：

```sql
SELECT e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE
FROM EMPLOYEES e, DEPARTMENTS d, LOCATIONS l, COUNTRIES c, JOBS j
WHERE e.DEPARTMENT_ID = d.DEPARTMENT_ID
AND d.LOCATION_ID = l.LOCATION_ID
AND l.COUNTRY_ID = c.COUNTRY_ID
AND e.JOB_ID = j.JOB_ID
AND c.COUNTRY_NAME = 'United States of America';
```

#### 分析：

这个查询语句从EMPLOYEES表、DEPARTMENTS表、LOCATIONS表、COUNTRIES表和JOBS表中检索员工的名字、姓氏和工作职称，使用逗号将这些表名分开，表示使用传统的SQL表连接方式，即内连接(inner join)，也称作等值连接(equi-join)，WHERE子句用于过滤结果集，将结果限制为满足以下条件的员工：

- 员工所在部门的DEPARTMENT_ID字段等于DEPARTMENTS表的DEPARTMENT_ID字段。
- 部门所在位置的LOCATION_ID字段等于LOCATIONS表的LOCATION_ID字段。
- 位置所在国家的COUNTRY_ID字段等于COUNTRIES表的COUNTRY_ID字段。
- 员工的工作ID字段等于JOBS表的JOB_ID字段。
- 所在国家为"United States of America"。

#### 输出结果：
```sql
FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Steven		     King
President

Neena		     Kochhar
Administration Vice President

Lex		     De Haan
Administration Vice President


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Jennifer	     Whalen
Administration Assistant

Nancy		     Greenberg
Finance Manager

Daniel		     Faviet
Accountant


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
John		     Chen
Accountant

Ismael		     Sciarra
Accountant

Jose Manuel	     Urman
Accountant


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Luis		     Popp
Accountant

Shelley 	     Higgins
Accounting Manager

William 	     Gietz
Public Accountant


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Den		     Raphaely
Purchasing Manager

Alexander	     Khoo
Purchasing Clerk

Shelli		     Baida
Purchasing Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Sigal		     Tobias
Purchasing Clerk

Guy		     Himuro
Purchasing Clerk

Karen		     Colmenares
Purchasing Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Matthew 	     Weiss
Stock Manager

Adam		     Fripp
Stock Manager

Payam		     Kaufling
Stock Manager


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Shanta		     Vollman
Stock Manager

Kevin		     Mourgos
Stock Manager

Julia		     Nayer
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Irene		     Mikkilineni
Stock Clerk

James		     Landry
Stock Clerk

Steven		     Markle
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Laura		     Bissot
Stock Clerk

Mozhe		     Atkinson
Stock Clerk

James		     Marlow
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
TJ		     Olson
Stock Clerk

Jason		     Mallin
Stock Clerk

Michael 	     Rogers
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Ki		     Gee
Stock Clerk

Hazel		     Philtanker
Stock Clerk

Renske		     Ladwig
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Stephen 	     Stiles
Stock Clerk

John		     Seo
Stock Clerk

Joshua		     Patel
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Trenna		     Rajs
Stock Clerk

Curtis		     Davies
Stock Clerk

Randall 	     Matos
Stock Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Peter		     Vargas
Stock Clerk

Winston 	     Taylor
Shipping Clerk

Jean		     Fleaur
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Martha		     Sullivan
Shipping Clerk

Girard		     Geoni
Shipping Clerk

Nandita 	     Sarchand
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Alexis		     Bull
Shipping Clerk

Julia		     Dellinger
Shipping Clerk

Anthony 	     Cabrio
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Kelly		     Chung
Shipping Clerk

Jennifer	     Dilly
Shipping Clerk

Timothy 	     Gates
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Randall 	     Perkins
Shipping Clerk

Sarah		     Bell
Shipping Clerk

Britney 	     Everett
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Samuel		     McCain
Shipping Clerk

Vance		     Jones
Shipping Clerk

Alana		     Walsh
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Kevin		     Feeney
Shipping Clerk

Donald		     OConnell
Shipping Clerk

Douglas 	     Grant
Shipping Clerk


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Alexander	     Hunold
Programmer

Bruce		     Ernst
Programmer

David		     Austin
Programmer


FIRST_NAME	     LAST_NAME
-------------------- -------------------------
JOB_TITLE
-----------------------------------
Valli		     Pataballa
Programmer

Diana		     Lorentz
Programmer


已选择 68 行。


执行计划
----------------------------------------------------------
Plan hash value: 1271915316

--------------------------------------------------------------------------------
------------------------------

| Id  | Operation				 | Name 	     | Rows  | B
ytes | Cost (%CPU)| Time     |

--------------------------------------------------------------------------------
------------------------------

|   0 | SELECT STATEMENT			 |		     |	   8 |
 632 |	   7   (0)| 00:00:01 |

|*  1 |  HASH JOIN				 |		     |	   8 |
 632 |	   7   (0)| 00:00:01 |

|   2 |   NESTED LOOPS				 |		     |	   8 |
 416 |	   4   (0)| 00:00:01 |

|   3 |    NESTED LOOPS 			 |		     |	  20 |
 416 |	   4   (0)| 00:00:01 |

|   4 |     NESTED LOOPS			 |		     |	   2 |
  50 |	   3   (0)| 00:00:01 |

|   5 |      NESTED LOOPS			 |		     |	   2 |
  36 |	   2   (0)| 00:00:01 |

|*  6 |       INDEX FULL SCAN			 | COUNTRY_C_ID_PK   |	   1 |
  12 |	   1   (0)| 00:00:01 |

|   7 |       TABLE ACCESS BY INDEX ROWID BATCHED| LOCATIONS	     |	   2 |
  12 |	   1   (0)| 00:00:01 |

|*  8 |        INDEX RANGE SCAN 		 | LOC_COUNTRY_IX    |	   2 |
     |	   0   (0)| 00:00:01 |

|   9 |      TABLE ACCESS BY INDEX ROWID BATCHED | DEPARTMENTS	     |	   1 |
   7 |	   1   (0)| 00:00:01 |

|* 10 |       INDEX RANGE SCAN			 | DEPT_LOCATION_IX  |	   4 |
     |	   0   (0)| 00:00:01 |

|* 11 |     INDEX RANGE SCAN			 | EMP_DEPARTMENT_IX |	  10 |
     |	   0   (0)| 00:00:01 |

|  12 |    TABLE ACCESS BY INDEX ROWID		 | EMPLOYEES	     |	   4 |
 108 |	   1   (0)| 00:00:01 |

|  13 |   TABLE ACCESS FULL			 | JOBS 	     |	  19 |
 513 |	   3   (0)| 00:00:01 |

--------------------------------------------------------------------------------
------------------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   1 - access("E"."JOB_ID"="J"."JOB_ID")
   6 - filter("C"."COUNTRY_NAME"='United States of America')
   8 - access("L"."COUNTRY_ID"="C"."COUNTRY_ID")
  10 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
  11 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")

Note
-----
   - this is an adaptive plan


统计信息
----------------------------------------------------------
	  1  recursive calls
	  0  db block gets
	 22  consistent gets
	  0  physical reads
	  0  redo size
       2987  bytes sent via SQL*Net to client
	652  bytes received via SQL*Net from client
	  6  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	 68  rows processed

```

# 优化指导

## 优化查询：
```sql
GENERAL INFORMATION SECTION
-------------------------------------------------------------------------------
Tuning Task Name   : staName27942
Tuning Task Owner  : HR
Tuning Task ID     : 31
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_51
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/21/2023 19:29:44
Completed at       : 03/21/2023 19:29:45

-------------------------------------------------------------------------------
Schema Name   : HR
Container Name: PDBORCL
SQL ID        : fa6tc0uxfvnj9
SQL Text      : SELECT e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE
                FROM EMPLOYEES e
                JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID
                JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID
                JOIN COUNTRIES c ON l.COUNTRY_ID = c.COUNTRY_ID
                JOIN JOBS j ON e.JOB_ID = j.JOB_ID
                WHERE c.COUNTRY_NAME = 'United States of America'

-------------------------------------------------------------------------------
There are no recommendations to improve the statement.

-------------------------------------------------------------------------------
EXPLAIN PLANS SECTION
-------------------------------------------------------------------------------

1- Original
-----------
Plan hash value: 1271915316

 
--------------------------------------------------------------------------------------------------------------
| Id  | Operation                                | Name              | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT                         |                   |     8 |   632 |     7   (0)| 00:00:01 |
|*  1 |  HASH JOIN                               |                   |     8 |   632 |     7   (0)| 00:00:01 |
|   2 |   NESTED LOOPS                           |                   |     8 |   416 |     4   (0)| 00:00:01 |
|   3 |    NESTED LOOPS                          |                   |    20 |   416 |     4   (0)| 00:00:01 |
|   4 |     NESTED LOOPS                         |                   |     2 |    50 |     3   (0)| 00:00:01 |
|   5 |      NESTED LOOPS                        |                   |     2 |    36 |     2   (0)| 00:00:01 |
|*  6 |       INDEX FULL SCAN                    | COUNTRY_C_ID_PK   |     1 |    12 |     1   (0)| 00:00:01 |
|   7 |       TABLE ACCESS BY INDEX ROWID BATCHED| LOCATIONS         |     2 |    12 |     1   (0)| 00:00:01 |
|*  8 |        INDEX RANGE SCAN                  | LOC_COUNTRY_IX    |     2 |       |     0   (0)| 00:00:01 |
|   9 |      TABLE ACCESS BY INDEX ROWID BATCHED | DEPARTMENTS       |     1 |     7 |     1   (0)| 00:00:01 |
|* 10 |       INDEX RANGE SCAN                   | DEPT_LOCATION_IX  |     4 |       |     0   (0)| 00:00:01 |
|* 11 |     INDEX RANGE SCAN                     | EMP_DEPARTMENT_IX |    10 |       |     0   (0)| 00:00:01 |
|  12 |    TABLE ACCESS BY INDEX ROWID           | EMPLOYEES         |     4 |   108 |     1   (0)| 00:00:01 |
|  13 |   TABLE ACCESS FULL                      | JOBS              |    19 |   513 |     3   (0)| 00:00:01 |
--------------------------------------------------------------------------------------------------------------
 
Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------
 
   1 - SEL$81719215
   6 - SEL$81719215 / C@SEL$3
   7 - SEL$81719215 / L@SEL$2
   8 - SEL$81719215 / L@SEL$2
   9 - SEL$81719215 / D@SEL$1
  10 - SEL$81719215 / D@SEL$1
  11 - SEL$81719215 / E@SEL$1
  12 - SEL$81719215 / E@SEL$1
  13 - SEL$81719215 / J@SEL$4
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   1 - access("E"."JOB_ID"="J"."JOB_ID")
   6 - filter("C"."COUNTRY_NAME"='United States of America')
   8 - access("L"."COUNTRY_ID"="C"."COUNTRY_ID")
  10 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
  11 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
 
Column Projection Information (identified by operation id):
-----------------------------------------------------------
 
   1 - (#keys=1) "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], 
       "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "J"."JOB_TITLE"[VARCHAR2,35]
   2 - (#keys=0) "D"."DEPARTMENT_ID"[NUMBER,22], "E"."FIRST_NAME"[VARCHAR2,20], 
       "E"."LAST_NAME"[VARCHAR2,25], "E"."JOB_ID"[VARCHAR2,10]
   3 - (#keys=0) "D"."DEPARTMENT_ID"[NUMBER,22], "E".ROWID[ROWID,10]
   4 - (#keys=0) "D"."DEPARTMENT_ID"[NUMBER,22]
   5 - (#keys=0) "L"."LOCATION_ID"[NUMBER,22]
   6 - "C"."COUNTRY_ID"[CHARACTER,2]
   7 - "L"."LOCATION_ID"[NUMBER,22]
   8 - "L".ROWID[ROWID,10]
   9 - "D"."DEPARTMENT_ID"[NUMBER,22]
  10 - "D".ROWID[ROWID,10]
  11 - "E".ROWID[ROWID,10]
  12 - "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."JOB_ID"[VARCHAR2,10]
  13 - (rowset=256) "J"."JOB_ID"[VARCHAR2,10], "J"."JOB_TITLE"[VARCHAR2,35]
 
Note
-----
   - this is an adaptive plan

-------------------------------------------------------------------------------
```
